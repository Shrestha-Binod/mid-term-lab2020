package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPositive() {
		assertTrue("The result is negative", Calculator.isPositive(5));
	}
	@Test
	public void testIsNegative() {
		assertFalse("The result is Positive", Calculator.isPositive(-5));
	}
	@Test
	public void testIsPositiveBoundaryIn() {
		assertTrue("The result is negative", Calculator.isPositive(0));
	}
	@Test
	public void testIsPositiveBoundaryOut() {
		assertFalse("The result is positive", Calculator.isPositive(-1));
	}
}
